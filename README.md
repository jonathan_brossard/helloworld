# Pipeline integration with MOABI demo

This repository illustrates how integration
between Gitlab and MOABI can be achieved thanks
to Gitlab pipelines and the MOABI REST API.

For more information, please refer to the MOABI SDK.

This repository, courtesy of MOABI: https://moabi.com

